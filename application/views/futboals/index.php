<h1 class="text-center" >LISTADO DE FUTBOLISTAS PROFESIONALES</h1>
<br>
<?php if ($futboals): ?>
    <table class="table table-striped
    table-bordered table-hover">
        <thead>
           <tr>
             <th>ID</th>
             <th>CEDULA</th>
             <th>PRIMER NOMBRE</th>
             <th>SEGUNDO APELLIDO</th>
             <th>TELEFONO</th>
             <th>DIRECCION</th>
             <th>ACCIONES</th>
           </tr>
        </thead>
        <tbody>
            <?php foreach ($futboals
            as $filaTemporal): ?>
              <tr>
                  <td>
                      <?php echo
                      $filaTemporal->id_fut; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->cedula_fut; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->primer_nombre_fut; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->segundo_apellido_fut; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->telefono_fut; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->direccion_fut; ?>
                  </td>

                  <td class="text-center" >
                    <a href="#" title="Editar Futboal" >
                      <i class="glyphicon glyphicon-pencil" > </i>
                    </a>
                      &nbsp;
                    <a href="<?php echo site_url('/futboals/eliminar/'); ?><?php echo
                    $filaTemporal->id_fut; ?>" title="Eliminar Futboal" style="color:red" >
                      <i class="glyphicon glyphicon-trash" > </i>
                    </a>
                  </td>

              </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
  <h1 class="text-center" >NO HAY DATOS DE FUTBOLISTAS ->INGRESA PORFAVOR</h1>
<?php endif; ?>
