<h1 class="text-center" >FORMULARIO PARA NUEVOS FUTBOLISTAS</h1>
<form class=""
action="<?php echo site_url(); ?>/futboals/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          name="cedula_fut" value="" id="cedula_fut">
      </div>
      <div class="col-md-4">
          <label for="">Primer Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el primer nombre"
          class="form-control"
          name="primer_nombre_fut" value="" id="primer_nombre_fut">
      </div>
      <div class="col-md-4">
        <label for="">Segundo Apellido:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el segundo apellido"
        class="form-control"
        name="segundo_apellido_fut" value="" id="segundo_apellido_fut">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
        <label for="">Teléfono:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el telefono"
        class="form-control"
        name="telefono_fut" value="" id="telefono_fut">
      </div>
      <div class="col-md-8">
          <label for="">Dirección:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          name="direccion_fut" value="" id="direccion_fut">
      </div>
    </div>

    <br>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/futboals/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
