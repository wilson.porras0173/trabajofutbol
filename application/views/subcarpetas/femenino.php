<center>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/imagenes/fotof1.png" alt="NOCARGADO">
      <div class="caption">
        <h3>CAMPEONATO 4 DE ABRIL</h3>
        <p>4 de enero del 2023</p>
        <p><a href="<?php echo base_url(); ?>/assets/imagenes/fotof1.png" class="btn btn-primary" role="button">VER</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/imagenes/fotof2.png" alt="NOCARGADO">
      <div class="caption">
        <h3>CAMPEONATO LAS MARGARITAS</h3>
        <p>6 de marzo del 2023</p>
        <p><a href="<?php echo base_url(); ?>/assets/imagenes/fotof2.png" class="btn btn-primary" role="button">VER</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/imagenes/fotof3.png" alt="NOCARGADO">
      <div class="caption">
        <h3>CAMPEONATO LOS AGUASEROS</h3>
        <p>8 de mayo del 2023</p>
        <p><a href="<?php echo base_url(); ?>/assets/imagenes/fotof3.png" class="btn btn-primary" role="button">VER</a></p>
      </div>
    </div>
  </div>
</div>
</center>


<center>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/imagenes/fotof4.png" alt="NOCARGADO">
      <div class="caption">
        <h3>CAMPEONATO LOS ANILLOS</h3>
        <p>10 de julio del 2023</p>
        <p><a href="<?php echo base_url(); ?>/assets/imagenes/fotof4.png" class="btn btn-primary" role="button">VER</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="<?php echo base_url(); ?>/assets/imagenes/fotof5.png" alt="NOCARGADO">
      <div class="caption">
        <h3>CAMPEONATO LAS ESMERALDAS</h3>
        <p>14 de septiembre del 2023</p>
        <p><a href="<?php echo base_url(); ?>/assets/imagenes/fotof5.png" class="btn btn-primary" role="button">VER</a></p>
      </div>
    </div>
</div>
<div class="col-sm-6 col-md-4">
  <div class="thumbnail">
    <img src="<?php echo base_url(); ?>/assets/imagenes/fotof6.png" alt="NOCARGADO">
    <div class="caption">
      <h3>CAMPEONATO LOS REYES</h3>
      <p>18 de noviembre del 2023</p>
      <p><a href="<?php echo base_url(); ?>/assets/imagenes/fotof6.png" class="btn btn-primary" role="button">VER</a></p>
    </div>
  </div>
</div>
</div>
</center>
