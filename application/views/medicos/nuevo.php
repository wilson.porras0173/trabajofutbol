<h1 class="text-center" >FORMULARIOS PARA NUEVOS MEDICOS</h1>
<form class=""
action="<?php echo site_url(); ?>/medicos/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          name="cedula_med" value="" id="cedula_med">
      </div>
      <div class="col-md-4">
          <label for="">Primer Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el primer apellido"
          class="form-control"
          name="primer_nombre_med" value="" id="primer_nombre_med">
      </div>
      <div class="col-md-4">
        <label for="">Segundo Apellido:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el segundo apellido"
        class="form-control"
        name="segundo_apellido_med" value="" id="segundo_apellido_med">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
        <label for="">Teléfono:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el telefono"
        class="form-control"
        name="telefono_med" value="" id="telefono_med">
      </div>
      <div class="col-md-8">
          <label for="">Dirección:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control"
          name="direccion_med" value="" id="direccion_med">
      </div>
    </div>

    <br>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/medicos/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
