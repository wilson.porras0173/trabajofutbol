<h1 class="text-center" >LISTA DE PERSONALES ACCESIBLES</h1>
<br>
<?php if ($personales): ?>
    <table class="table table-striped
    table-bordered table-hover">
        <thead>
           <tr>
             <th>ID</th>
             <th>CEDULA</th>
             <th>PRIMER NOMBRE</th>
             <th>SEGUNDO APELLIDO</th>
             <th>TELEFONO</th>
             <th>DIRECCION</th>
             <th>ACCIONES</th>
           </tr>
        </thead>
        <tbody>
            <?php foreach ($personales
            as $filaTemporal): ?>
              <tr>
                  <td>
                      <?php echo
                      $filaTemporal->id_per; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->cedula_per; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->primer_nombre_per; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->segundo_apellido_per; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->telefono_per; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->direccion_per; ?>
                  </td>

                  <td class="text-center" >
                    <a href="#" title="Editar Personal" >
                      <i class="glyphicon glyphicon-pencil" > </i>
                    </a>
                      &nbsp;
                    <a href="<?php echo site_url('/personales/eliminar/'); ?><?php echo
                    $filaTemporal->id_per; ?>" title="Eliminar Personal" style="color:red" >
                      <i class="glyphicon glyphicon-trash" > </i>
                    </a>
                  </td>

              </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
  <h1 class="text-center">NO HAY DATOS DE PERSONALES ->INGRESA PORFAVOR</h1>
<?php endif; ?>
