<?php

    class Personales extends CI_Controller
    {
        function __construct()
        {
          parent::__construct();
          //Cargar modelo
          $this->load->model('Personal');
        }

    public function index(){
      //LINEA DE CODIGO NUEVOS
      $data['personales']=$this->Personal->obtenerTodos();
      //FIN
      $this->load->view('header');
      $this->load->view('personales/index',$data);
      $this->load->view('footer');

    }
    public function nuevo(){
      $this->load->view('header');
      $this->load->view('personales/nuevo');
      $this->load->view('footer');

    }


    public function guardar(){
      $datosNuevoPersonal=array(
        "cedula_per"=>$this->input->post('cedula_per'),
        "primer_nombre_per"=>$this->input->post('primer_nombre_per'),
        "segundo_apellido_per"=>$this->input->post('segundo_apellido_per'),
        "telefono_per"=>$this->input->post('telefono_per'),
        "direccion_per"=>$this->input->post('direccion_per')
  );
  if($this->Personal->insertar($datosNuevoPersonal)){
redirect('personales/index');
}else{
echo "<h1>ERROR AL INSERTAR</h1>";
}
}

//funcion para eliminar instructores
public function eliminar($id_per){
  if ($this->Personal->borrar($id_per)) {//invocando el modelo
    redirect('personales/index');
  } else {
    echo "ERROR AL BORRAR ;) ";
  }

}

  }//cierre de la clase

 ?>
