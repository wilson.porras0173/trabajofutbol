<?php

    class Listados extends CI_Controller
    {
        function __construct()
        {
          parent::__construct();
          //Cargar modelo
          $this->load->model('Listadoo');
        }

    public function index(){
      $this->load->view('header');
      $this->load->view('instructores/index0');
      $this->load->view('footer');

    }
    public function nuevo(){
      $this->load->view('header');
      $this->load->view('instructores/nuevo');
      $this->load->view('footer');

    }
    public function guardar(){
      $datosNuevoListadoo=array(
        "cedula_lis"=>$this->input->post('cedula_lis'),
        "primer_apellido_lis"=>$this->input->post('primer_apellido_lis'),
        "segundo_apellido_lis"=>$this->input->post('segundo_apellido_lis'),
        "nombre_lis"=>$this->input->post('nombre_lis'),
        "titulo_lis"=>$this->input->post('titulo_lis'),
        "telefono_lis"=>$this->input->post('telefono_lis'),
        "direccion_lis"=>$this->input->post('direccion_lis')
      );

      $this->Listadoo->insertar($datosNuevoListadoo);
    }
  }//cierre de la clase

 ?>
