<?php

    class Medicos extends CI_Controller
    {
        function __construct()
        {
          parent::__construct();
          //Cargar modelo
          $this->load->model('Medico');
        }

    public function index(){
      //LINEA DE CODIGO NUEVOS
      $data['medicos']=$this->Medico->obtenerTodos();
      //FIN
      $this->load->view('header');
      $this->load->view('medicos/index',$data);
      $this->load->view('footer');

    }
    public function nuevo(){
      $this->load->view('header');
      $this->load->view('medicos/nuevo');
      $this->load->view('footer');

    }

    public function guardar(){
      $datosNuevoMedico=array(
        "cedula_med"=>$this->input->post('cedula_med'),
        "primer_nombre_med"=>$this->input->post('primer_nombre_med'),
        "segundo_apellido_med"=>$this->input->post('segundo_apellido_med'),
        "telefono_med"=>$this->input->post('telefono_med'),
        "direccion_med"=>$this->input->post('direccion_med')
  );

  if($this->Medico->insertar($datosNuevoMedico)){
redirect('medicos/index');
}else{
echo "<h1>ERROR AL INSERTAR</h1>";
}
}

//funcion para eliminar instructores
public function eliminar($id_med){
  if ($this->Medico->borrar($id_med)) {//invocando el modelo
    redirect('medicos/index');
  } else {
    echo "ERROR AL BORRAR ;) ";
  }

}

  }//cierre de la clase

 ?>
