<?php
  /**
   *
   */
  class Personal extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }
    //funcion para insertar un instructot
    function insertar($datos){
      //ACTIVE_RECORD > en CodeIgniter
      return $this->db->insert("personal",$datos);
      //Inseccion sQL
    }
    //FUNCION PARA CONSULTAR INSTRUCTORES LINEAS DE CODIGOS NUEVOS
    function obtenerTodos(){
      $listadoPersonales=$this->db->get("personal");
      //VALIDAR PARA QUE NO DE ERRORES
      //SIEMPRE VALIDAR CON UN IF PARA QUE NO HAYA ERRORES
      if($listadoPersonales->num_rows()>0) { // SI HAY DATOS
        return $listadoPersonales->result();
      }else { // NO HAY DATOS
        return false;
      }
    }

    function borrar($id_per){
  //delete from instructor where id_ins=
  $this->db->where("id_per", $id_per);
  return $this->db->delete("personal");
}

  }//cierre de la funcion



 ?>
