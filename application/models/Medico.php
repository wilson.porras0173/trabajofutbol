<?php
  /**
   *
   */
  class Medico extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }
    //funcion para insertar un instructot
    function insertar($datos){
      //ACTIVE_RECORD > en CodeIgniter
      return $this->db->insert("medico",$datos);
      //Inseccion sQL
    }
    //FUNCION PARA CONSULTAR INSTRUCTORES LINEAS DE CODIGOS NUEVOS
    function obtenerTodos(){
      $listadoMedicos=$this->db->get("medico");
      //VALIDAR PARA QUE NO DE ERRORES
      //SIEMPRE VALIDAR CON UN IF PARA QUE NO HAYA ERRORES
      if($listadoMedicos->num_rows()>0) { // SI HAY DATOS
        return $listadoMedicos->result();
      }else { // NO HAY DATOS
        return false;
      }
    }
    function borrar($id_med){
  //delete from instructor where id_ins=
  $this->db->where("id_med", $id_med);
  return $this->db->delete("medico");
}
  }//cierre de la funcion



 ?>
